select 'drop, create, use database, create tables, display data:' as '';
do sleep(5);

Drop schema if exists arw18bj;
create schema if not exists arw18bj;
show warnings;
use arw18bj;


-- Table person -------
drop table if exists person;
create table if not exists person(
	per_id smallint unsigned not null auto_increment,
    per_ssn binary(64) null,
    per_salt binary(64) null comment 'only for demo. DO NOT use salt name in prod',
    per_fname varchar(15) not null,
    per_lname varchar(30) not null,
    per_street varchar(30) not null,
    per_city varchar(30) not null,
    per_state char(2) not null,
    per_zip char(9) not null,
    per_email varchar(100) not null,
    per_dob date not null,
    per_type ENUM('a','c','j'),
    per_notes varchar(255) null,
    primary key (per_id),
    unique index ux_per_ssn (per_ssn ASC)
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table attorney -------
drop table if exists attorney;
create table if not exists attorney(
	per_id smallint unsigned not null,
    aty_start_date date not null,
    aty_end_date date default null,
    aty_hourly_rate decimal(5,2) not null,
    aty_years_in_practice tinyint not null,
    aty_notes varchar(255) null default null,
    primary key (per_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_attorney_person
    foreign key (per_id)
    references person (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table client -------
drop table if exists client;
create table if not exists client(
	per_id smallint unsigned not null,
    cli_notes varchar(255) null default null,
    primary key (per_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_client_person
    foreign key (per_id)
    references person (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table court -------
drop table if exists court;
create table if not exists court(
	crt_id tinyint unsigned not null auto_increment,
    crt_name varchar(45) not null,
    crt_street varchar(30) not null,
    crt_city varchar(30) not null,
    crt_state char(2) not null,
    crt_zip char(9) not null,
    crt_phone bigint not null,
    crt_email varchar(100) not null,
    crt_url varchar(100) not null,
    crt_notes varchar(255) null,
    primary key (crt_id)
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table judge -------
drop table if exists judge;
create table if not exists judge(
	per_id smallint unsigned not null,
    crt_id tinyint unsigned null default null,
    jud_salary decimal(8,2) not null,
    jud_years_in_practice tinyint unsigned not null,
    jud_notes varchar(255) null default null,
    primary key (per_id),
    
    index idx_per_id (per_id asc),
    index idx_crt_id (crt_id asc),
    
    constraint fk_judge_person
    foreign key (per_id)
    references person (per_id)
    on delete no action
    on update cascade,
    
    constraint fk_judge_court
    foreign key (crt_id)
    references court (crt_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table judge_hist -------
drop table if exists judge_hist;
create table if not exists judge_hist(
	jhs_id smallint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    jhs_crt_id tinyint null,
    jhs_date timestamp not null default current_timestamp,
    jhs_type enum('i','u','d') not null default 'i',
    jhs_salary decimal(8,2) not null,
    jhs_notes varchar(255) null default null,
    primary key (jhs_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_judge_hist_judge
    foreign key (per_id)
    references judge (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table case -------
drop table if exists `case`;
create table if not exists `case`(
	cse_id smallint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    cse_type varchar(45) not null,
    cse_description text not null,
    cse_start_date date not null,
    cse_end_date date null,
    cse_notes varchar(255) null,
    primary key (cse_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_court_case_judge
    foreign key (per_id)
    references judge (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table bar -------
drop table if exists bar;
create table if not exists bar(
	bar_id tinyint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    bar_name varchar(45) not null,    
    bar_notes varchar(255) null,
    primary key (bar_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_bar_attorney
    foreign key (per_id)
    references attorney (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table specialty -------
drop table if exists specialty;
create table if not exists specialty(
	spc_id tinyint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    spc_type varchar(45) not null,    
    spc_notes varchar(255) null,
    primary key (spc_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_specialty_attorney
    foreign key (per_id)
    references attorney (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table assignment -------
drop table if exists assignment;
create table if not exists assignment(
	asn_id smallint unsigned not null auto_increment,
    per_cid smallint unsigned not null,
    per_aid smallint unsigned not null,
    cse_id smallint unsigned not null,
    asn_notes varchar(255) null,
    primary key (asn_id),
    
    index idx_per_cid (per_cid asc),
    index idx_per_aid (per_aid asc),
    index idx_cse_id (cse_id asc),
    
    constraint fk_assign_case
    foreign key (cse_id)
    references `case` (cse_id)
    on delete no action
    on update cascade,
    
    constraint fk_assignment_client
    foreign key (per_cid)
    references client (per_id)
    on delete no action
    on update cascade,
    
    constraint fk_assignment_attorney
    foreign key (per_aid)
    references attorney (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table phone  -------
drop table if exists phone;
create table if not exists phone(
	phn_id smallint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    phn_num bigint unsigned not null,
    phn_type enum('h','c','w','f') not null comment 'home, cell, work, fax',
    phn_notes varchar(255) null,
    primary key (phn_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_phone_person
    foreign key (per_id)
    references person (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

#inserts for tables#

-- Person table -------
Start transaction;

insert into person
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
values
(NULL, NULL, NULL, 'John', 'James', '123 Apple Rd.', 'Tallahassee', 'FL', 111111111, 'jj@gmail.com', '1999-01-24', 'c', NULL), 
(NULL, NULL, NULL, 'Jane', 'Doe', '345 Banana Rd.', 'Tallahassee', 'FL', 222222222, 'jd@gmail.com', '1997-04-27', 'j', NULL), 
(NULL, NULL, NULL, 'Jessica', 'Simpson', '567 Coconut Rd.', 'Tallahassee', 'FL', 333333333, 'js@gmail.com', '1997-08-23', 'c', NULL), 
(NULL, NULL, NULL, 'Happy', 'Gilmore', '789 Dragonfruit Dr.', 'Tallahassee', 'FL', 444444444, 'hg@gmail.com', '1995-10-10', 'a', NULL), 
(NULL, NULL, NULL, 'Hungry', 'Howie', '901 Efruit Rd.', 'Tallahassee', 'FL', 555555555, 'hh@gmail.com', '1999-05-05', 'c', NULL), 
(NULL, NULL, NULL, 'Ronald', 'McDonald', '234 Fruit Ave.', 'Tallahassee', 'FL', 666666666, 'rm@gmail.com', '1994-04-08', 'c', NULL), 
(NULL, NULL, NULL, 'Wendy', 'Lastname', '456 Granola St.', 'Jacksonville', 'FL', 777777777, 'wl@gmail.com', '1996-02-19', 'a', NULL), 
(NULL, NULL, NULL, 'Person', 'Being', '678 Horchata Rd.', 'Jacksonville', 'FL', 888888888, 'pb@gmail.com', '1998-08-19', 'a', NULL), 
(NULL, NULL, NULL, 'Stan', 'Marsh', '890 Icecream Dr.', 'Jacksonville', 'FL', 999999999, 'sm@gmail.com', '1996-01-20', 'j', NULL), 
(NULL, NULL, NULL, 'Randy', 'Marsh', '012 Jackfruit Dr.', 'Jacksonville', 'FL', 123456789, 'rm@gmail.com', '1996-11-24', 'a', NULL), 
(NULL, NULL, NULL, 'Eric', 'Cartmann', '111 Kangaroo Dr.', 'Miami', 'FL', 234567891, 'ec@gmail.com', '1999-12-11', 'j', NULL), 
(NULL, NULL, NULL, 'Kyle', 'Brouflouski', '222 Limo Dr.', 'Miami', 'FL', 345678912, 'kb@gmail.com', '1999-08-01', 'j', NULL), 
(NULL, NULL, NULL, 'Kenny', 'McCormick', '333 Monkey Dr.', 'Miami', 'FL', 456789123, 'dm@gmail.com', '1991-01-22', 'c', NULL), 
(NULL, NULL, NULL, 'Joseph', 'Monaghan', '444 Nobody Dr.', 'Miami', 'FL', 567891234, 'jm@gmail.com', '1999-03-27', 'a', NULL), 
(NULL, NULL, NULL, 'Brandon', 'Cooks', '555 BC Dr.', 'Tampa', 'FL', 678912345, 'bc@gmail.com', '1991-03-12', 'a', NULL);

commit;

-- phone table -------
Start transaction;

insert into phone
(phn_id, per_id, phn_num, phn_type, phn_notes)
values
(null, 1, 1234567890, 'h', null),
(null, 2, 2345678901, 'c', null),
(null, 3, 3456789012, 'w', null),
(null, 4, 4567890123, 'c', null),
(null, 5, 5678901234, 'f', null),
(null, 6, 6789012345, 'w', null),
(null, 7, 7890123456, 'h', null),
(null, 8, 8901234567, 'w', null),
(null, 9, 9012345678, 'w', null),
(null, 10, 1111111111, 'h', null),
(null, 11, 2222222222, 'w', null),
(null, 12, 3333333333, 'c', null),
(null, 13, 4444444444, 'w', null),
(null, 14, 5555555555, 'w', null),
(null, 15, 6666666666, 'w', null);

commit;

-- client table -------
Start transaction;

insert into client
(per_id, cli_notes)
values
(1, null),
(2, null),
(3, null),
(4, null),
(5, null);

commit;

-- attorney table -------
Start transaction;

insert into attorney
(per_id, aty_start_date, aty_end_date, aty_hourly_rate, aty_years_in_practice, aty_notes)
values
(6, '2020-05-16', null, 100, 15, null),
(7, '2019-04-27', null, 150, 20,null),
(8, '2019-10-19', null, 75, 5,null),
(9, '2018-03-25', null, 125, 10,null),
(10, '2017-08-21', null, 75, 20,null);

commit;

-- bar table -------
Start transaction;

insert into bar
(bar_id, per_id, bar_name, bar_notes)
values
(null, 6, 'Florida', null),
(null, 7, 'Colorado', null),
(null, 8, 'Georgia', null),
(null, 9, 'California', null),
(null, 10, 'South Carolina', null),
(null, 6, 'Missouri', null),
(null, 7, 'Utah', null),
(null, 8, 'North Carolina', null),
(null, 9, 'New Jersy', null),
(null, 10, 'New York', null),
(null, 6, 'Alabama', null),
(null, 7, 'West Virginia', null),
(null, 8, 'Idaho', null),
(null, 9, 'Hawaii', null),
(null, 10, 'Michigan', null),
(null, 6, 'Iowa', null),
(null, 7, 'Texas', null),
(null, 8, 'Kansas', null),
(null, 9, 'Arkansas', null);

commit;

-- specialty table -------
Start transaction;

insert into specialty
(spc_id, per_id, spc_type, spc_notes)
values
(null, 6, 'Corporate', null),
(null, 7, 'Incedent', null),
(null, 8, 'Financial', null),
(null, 9, 'Judicial', null),
(null, 10, 'Civil', null),
(null, 6, 'Criminal', null),
(null, 7, 'Murder', null),
(null, 8, 'Cold cases', null),
(null, 9, 'Buisness', null);

commit;

-- court table -------
Start transaction;

insert into court
(crt_id, crt_name, crt_street, crt_city, crt_state, crt_zip, crt_phone, crt_email, crt_url, crt_notes)
values
(null, 'Miami Traffic Court', '300 S 42nd Ave', 'Miami', 'FL', 323092162, 3056061234, 'MTC@court.com', 'https://www.miamitrafficcourt.com', null),
(null, 'Broward County Court of affairs', '200 W 43 St.', 'Ft. Lauderdale', 'FL', 898989292, 8891234100, 'BCCA@court.com', 'https://www.BrowardCountyCourtofaffairs.com', null),
(null, 'Orlando judicial courthouse', '500 N Capitol Rd.', 'Orlando', 'FL', 902315292, 8500098265, 'OJC@court.com', 'https://www.Orlandojudicialcourthouse.com', null),
(null, 'Jacksonville Judicial courthouse', '420 Smokey Blvd.', 'Jacksonville', 'FL', 309182148, 1234362000, 'JJC@court.com', 'https://www.JacksonvilleJudicialcourthouse.com', null),
(null, 'Tallahasse Dicstric of appeals', '900 S Monroe St.', 'Tallahassee', 'FL', 320923463, 3000888800, 'TDA@court.com', 'https://www.TallahasseDicstricofappeals.com', null);

commit;

-- judge table -------
Start transaction;

insert into judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
values
(11, 4, 190000, 9, null),
(12, 3, 150000, 5, null),
(13, 2, 170000, 7, null),
(14, 4, 190000, 9, null),
(15, 1, 220000, 11, null);

commit;

-- judge_hist table -------
Start transaction;

insert into judge_hist
(jhs_id, per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
values
(null, 11, 3, '2010-01-16', 'i', 100000, null),
(null, 12, 2, '2013-03-12', 'i', 100000, null),
(null, 13, 5, '2009-05-01', 'i', 120000, null),
(null, 13, 4, '2010-09-18', 'i', 125000, null),
(null, 14, 4, '2011-10-29', 'i', 125000, null),
(null, 15, 1, '2012-12-18', 'i', 125000, null),
(null, 11, 5, '2009-10-14', 'i', 180000, null),
(null, 12, 4, '2010-01-18', 'i', 185000, null),
(null, 14, 3, '2011-02-10', 'i', 180000, null);

commit;

-- `case` table -------
Start transaction;

insert into `case`
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
values
(null, 13, 'civil', 'User fell into a pothole, broke leg and arm', '2016-09-09', null, null),
(null, 12, 'criminal', 'User was caught with 10 pounds of cocaine', '2000-11-12', '2011-10-12', null),
(null, 14, 'criminal', 'User shot neighbor over leaf in pool', '2001-05-11', '2021-09-22', null),
(null, 11, 'criminal', 'User blew up a subway sandwitch shop', '2019-05-17', null, null),
(null, 13, 'criminal', 'Client is found distributing 10 pounds of Marijuanna', '2018-06-10', null, null),
(null, 14, 'criminal', 'User is smuggling woman from korea into the US, to force them to work', '2009-01-19', '2010-01-10', null),
(null, 12, 'criminal', 'User was caught relieving himself on a Mcdonalds sign, while intoxicated', '2011-03-13', null, null),
(null, 15, 'civil', 'User stole 10 cents a day over the course of 10 years, $100000 total', '2011-01-13', '2021-04-28', null);

commit;

-- assignment table -------
Start transaction;

insert into assignment
(asn_id, per_cid, per_aid, cse_id, asn_notes)
values
(null, 1, 5, 2, null),
(null, 2, 6, 9, null),
(null, 3, 5, 3, null),
(null, 4, 7, 1, null),
(null, 5, 8, 2, null),
(null, 1, 9, 5, null),
(null, 2, 3, 8, null),
(null, 3, 9, 3, null),
(null, 4, 2, 1, null),
(null, 5, 8, 1, null),
(null, 4, 8, 2, null);

commit;

DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN
DECLARE x, y INT;
SET x = 1;
select count(*) into y from person;

WHILE x <= y DO
SET @salt=RANDOM_BYTES(64);
SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111;
SET @ssn=unhex(sha2(concat(@salt, @ran_num), 512));

update person
set per_ssn=@ssn, per_salt=@salt
where per_id=x;
SET x = x + 1;
END WHILE;
END$$
DElIMITER ;
call CreatePersonSSN();