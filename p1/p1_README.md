# LIS3781 Advanced Database Management

## Avery Withers

### Project 1 Requirements:

*Three Parts:*

1. Create tables and inserts using mysql db
2. Reverse engineer to cci server
3. Provide code via wmb file and screenshot of populated person table

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of populated person table
* SQL code

#### Assignment Screenshots:

*Screenshot of P1 ERD*:

![P1 ERD](img/p1_erd.png)

*Screenshot of populated person table*:

![P1 Person Table](img/p1_person_table.png)

#### Assignment Links

*P1 sql file*:

[P1 sql](p1.sql)

*P1 wmb file*:

[P1 wmb](p1.wmb)