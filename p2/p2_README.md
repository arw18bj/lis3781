# LIS3781 Advanced Database Management

## Avery Withers

### Project 2 Requirements:

*Three Parts:*

1. Take screenshot of at least one MongoDB shell command(s), (e.g., show collections); 
2. Take screenshot: At least *one* required report (i.e., exercise below), and JSON code solution.  
3. Upload to bitbucket

#### README.md file should include the following items:

*  Screenshot of at least one MongoDB shell command(s), (e.g., show collections); 
* Screenshot: At least *one* required report (i.e., exercise below), and JSON code solution.  
* Bitbucket repo links: *Your* lis3781 Bitbucket repo link