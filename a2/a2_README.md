# LIS3781 Advanced Database Management

## Avery Withers

### Assignment 2 Requirements:

*Four Parts:*

1. Create database, tables, inserts using SQL
2. Create users and grant permissions on local server
3. Provide screenshots of code and populated tables
4. Forward engineer to CCI server

#### README.md file should include the following items:

* Screenshot of SQL code
* Screenshot of populated tables;

#### Assignment Screenshots:

*Screenshot of A2 SQL Code*:

![A2 SQL](img/a2_code.png)

*Screenshot of A2 Populated Tables*:

![A2 Tables](img/a2_sql_statements.png)