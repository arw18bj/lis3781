# LIS3781 Advanced Database Management

## Avery Withers

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/a1_README.md "My A1 README.md file")
    - Install AMPPS (mysql for mac)
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git commands descriptions

2. [A2 README.md](a2/a2_README.md "My A2 README.md file")
    - Create database, tables, inserts using SQL
    - Create users and grant permissions on local server
    - Provide screenshots of code and populated tables
    - Forward engineer to CCI server

3. [A3 README.md](a3/a3_README.md "My A3 README.md file")
    - Log into Oracle via RemoteLabs
    - Connect to class Oracle server
    - Create and populate tables
    - Forward engineer to class Oracle server
    - Provide screenshots of code and populated tables

4. [A4 README.md](a4/a4_README.md "My A4 README.md file")
    - Open MS SQL Server
    - Create tables & inserts
    - Execute code
    - Take screenshot of ERD

5. [A5 README.md](a5/a5_README.md "My A5 README.md file")
    - Open/Login MS SQL Server
    - Create tables & inserts for region, state, city, time, & sale
    - Execute code
    - Take screenshot of ERD and at least one report

6. [P README.md](p1/p1_README.md "My P README.md file")
    - Create tables and inserts using mysql db
    - Reverse engineer to cci server
    - Provide code via wmb file and screenshot of populated person table