# LIS3781 Advanced Database Management

## Avery Withers

### Assignment 5 Requirements:

*Four Parts:*

1. Open/Login MS SQL Server
2. Create tables & inserts for region, state, city, time, & sale
3. Execute code
4. Take screenshot of ERD and at least one report

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of at least one report
* Bitbucket repo link

#### Assignment Screenshots:

*Screenshot of A5 ERD*:

![A5 ERD](img/a5_erd.png)

*Screenshot of A5 ERD*:

![A5 Report 1](img/a5_report1.png)