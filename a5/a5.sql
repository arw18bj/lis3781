use master;
GO

if exists (select name from master.dbo.sysdatabases where name = N'arw18bj')
drop database arw18bj;
GO

if not exists (select name from master.dbo.sysdatabases where name = N'arw18bj')
create database arw18bj;
GO

use arw18bj;
GO

-- -------------------------------------
-- Table person
-- -------------------------------------

if OBJECT_ID (N'dbo.person', N'U') is not null
drop table dbo.person;
GO

create table dbo.person(
    per_id smallint not null identity(1,1),
    per_ssn binary(64) null,
    per_salt binary(64) null,
    per_fname varchar(15) not null,
    per_lname varchar(30) not null,
    per_gender char(1) not null check (per_gender IN('m','f')),
    per_dob date not null,
    per_street varchar(30) not null,
    per_city varchar(30) not null,
    per_state char(2) not null default 'FL',
    per_zip int not null check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email varchar(100) null,
    per_type char(1) not null check (per_type in('c','s')),
    per_notes varchar(45) null,
    primary key (per_id),

    constraint ux_per_ssn unique nonclustered (per_ssn ASC)
);
GO

-- -------------------------------------
-- Table phone
-- -------------------------------------
if OBJECT_ID (N'dbo.phone', N'U') is not null
drop table dbo.phone;
GO

create table dbo.phone(
    phn_id smallint not null identity(1,1),
    per_id smallint not null,
    phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type char(1) not null check (phn_type in('h','c','w','f')),
    phn_notes varchar(255) null,
    primary key (phn_id),

    constraint fk_phone_person
    foreign key (per_id)
    references dbo.person (per_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table customer
-- -------------------------------------
if OBJECT_ID (N'dbo.customer', N'U') is not null
drop table dbo.customer;
GO

create table dbo.customer(
    per_id smallint not null,
    cus_balance decimal(7,2) not null check (cus_balance >= 0),
    cus_total_sales decimal(7,2) not null check (cus_total_sales >= 0),
    cus_notes varchar(45) null,
    primary key (per_id),

    constraint fk_customer_person
    foreign key (per_id)
    references dbo.person (per_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table slsrep
-- -------------------------------------
if OBJECT_ID (N'dbo.slsrep', N'U') is not null
drop table dbo.slsrep;
GO

create table dbo.slsrep(
    per_id smallint not null,
    srp_yr_sales_goal decimal(8,2) not null check (srp_yr_sales_goal >= 0),
    srp_ytd_sales decimal(8,2) not null check (srp_ytd_sales >= 0),
    srp_ytd_comm decimal(7,2) not null check (srp_ytd_comm >= 0),
    srp_notes varchar(45) null,
    primary key (per_id),

    constraint fk_slsrep_person
    foreign key (per_id)
    references dbo.person (per_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table srp_hist
-- -------------------------------------

if OBJECT_ID (N'dbo.srp_hist', N'U') is not null
drop table dbo.srp_hist;
GO

create table dbo.srp_hist(
    sht_id smallint not null identity(1,1),
    per_id smallint not null,
    sht_type char(1) not null check (sht_type in('i','u','d')),
    sht_modified datetime not null,
    sht_modifier varchar(45) not null default system_user,
    sht_date date not null default getDate(),
    sht_yr_sales_goal decimal(8,2) not null check (sht_yr_sales_goal >= 0),
    sht_ytd_sales decimal(8,2) not null check (sht_ytd_sales >= 0),
    sht_ytd_comm decimal(7,2) not null check (sht_ytd_comm >= 0),
    sht_notes varchar(45) null,
    primary key (sht_id),

    constraint fk_srp_hist_slsrep
    foreign key (per_id)
    references dbo.slsrep (per_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table contact
-- -------------------------------------
if OBJECT_ID (N'dbo.contact', N'U') is not null
drop table dbo.contact;
GO

create table dbo.contact(
    cnt_id int not null identity(1,1),
    per_cid smallint not null,
    per_sid smallint not null,
    cnt_date datetime not null,
    cnt_notes varchar(255) null,
    primary key (cnt_id),

    constraint fk_contact_customer
    foreign key (per_cid)
    references dbo.customer (per_id)
    on delete cascade
    on update cascade,
    
    constraint fk_contact_slsrep
    foreign key (per_sid)
    references dbo.slsrep (per_id)
    on delete no action
    on update no action
);
GO

-- -------------------------------------
-- Table [order]
-- -------------------------------------
if OBJECT_ID (N'dbo.[order]', N'U') is not null
drop table dbo.[order];
GO

create table dbo.[order](
    ord_id int not null identity(1,1),
    cnt_id int not null,
    ord_placed_date datetime not null,
    ord_filled_date datetime null,
    ord_notes varchar(255) null,
    primary key (ord_id),

    constraint fk_order_contact
    foreign key (cnt_id)
    references dbo.contact (cnt_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table region
-- -------------------------------------

if OBJECT_ID (N'dbo.region', N'U') is not null
drop table dbo.region;
GO

create table dbo.region(
    reg_id tinyint not null identity(1,1),
    reg_name char(1) not null, -- n,e,s,w,c (north, east, south, west, central)
    reg_notes varchar(255) null,
    primary key (reg_id)
);
GO

-- -------------------------------------
-- Table state
-- -------------------------------------

if OBJECT_ID (N'dbo.state', N'U') is not null
drop table dbo.state;
GO

create table dbo.state(
    ste_id tinyint not null identity(1,1),
    reg_id tinyint not null,
    ste_name char(2) not null default 'FL',
    ste_notes varchar(255) null,
    primary key (ste_id),

    constraint fk_state_region
    foreign key (reg_id)
    references dbo.region (reg_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table city 
-- -------------------------------------

if OBJECT_ID (N'dbo.city', N'U') is not null
drop table dbo.city;
GO

create table dbo.city(
    city_id smallint not null identity(1,1),
    ste_id tinyint not null,
    city_name varchar(30) not null,
    city_notes varchar(255) null,
    primary key (city_id),

    constraint fk_city_state
    foreign key (ste_id)
    references dbo.state (ste_id)
    on delete cascade
    on update cascade
);
GO


-- -------------------------------------
-- Table store
-- -------------------------------------
if OBJECT_ID (N'dbo.store', N'U') is not null
drop table dbo.store;
GO

create table dbo.store(
    str_id smallint not null identity(1,1),
    str_name varchar(45) not null,
    str_street varchar(30) not null,
    str_city varchar(30) not null,
    str_state char(2) not null default 'FL',
    str_zip int not null check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_phone bigint not null check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_email varchar(100) not null,
    str_url varchar(100) not null,
    str_notes varchar(255) null,
    primary key (str_id),
);
GO

-- -------------------------------------
-- Table invoice
-- -------------------------------------
if OBJECT_ID (N'dbo.invoice', N'U') is not null
drop table dbo.invoice;
GO

create table dbo.invoice(
    inv_id int not null identity(1,1),
    ord_id int not null,
    str_id smallint not null,
    inv_date datetime not null,
    inv_total decimal(8,2) not null check (inv_total >= 0),
    inv_paid bit not null,
    inv_notes varchar(255) null,
    primary key (inv_id),

    constraint ux_ord_id unique nonclustered (ord_id asc),

    constraint fk_invoice_order
    foreign key (ord_id)
    references dbo.[order] (ord_id)
    on delete cascade
    on update cascade, 

    constraint fk_invoice_store
    foreign key (str_id)
    references dbo.store (str_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table payment
-- -------------------------------------
if OBJECT_ID (N'dbo.payment', N'U') is not null
drop table dbo.contact;
GO

create table dbo.payment(
    pay_id int not null identity(1,1),
    inv_id int not null,
    pay_date datetime not null,
    pay_amt decimal(7,2) not null check (pay_amt >= 0),
    pay_notes varchar(255) null,
    primary key (pay_id),

    constraint fk_payment_invoice
    foreign key (inv_id)
    references dbo.invoice (inv_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table vendor
-- -------------------------------------
if OBJECT_ID (N'dbo.vendor', N'U') is not null
drop table dbo.vendor;
GO

create table dbo.vendor(
    ven_id smallint not null identity(1,1),
    ven_name varchar(45) not null,
    ven_street varchar(30) not null,
    ven_city varchar(30) not null,
    ven_state char(2) not null default 'FL',
    ven_zip int not null check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_phone bigint not null check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email varchar(100) not null,
    ven_url varchar(100) not null,
    ven_notes varchar(255) null,
    primary key (ven_id)
);
GO

-- -------------------------------------
-- Table product
-- -------------------------------------
if OBJECT_ID (N'dbo.product', N'U') is not null
drop table dbo.product;
GO

create table dbo.product(
    pro_id smallint not null identity(1,1),
    ven_id smallint not null,
    pro_name varchar(30) not null,
    pro_descript varchar(45) null,
    pro_weight float not null check (pro_weight >= 0),
    pro_qoh smallint not null check (pro_qoh >= 0),
    pro_cost decimal(7,2) not null check (pro_cost >= 0),
    pro_price decimal(7,2) not null check (pro_price >= 0),
    pro_discount decimal(3,0) null,
    pro_notes varchar(255) null,
    primary key (pro_id),

    constraint fk_product_vendor
    foreign key (ven_id)
    references dbo.vendor (ven_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table product_hist
-- -------------------------------------
if OBJECT_ID (N'dbo.product_hist', N'U') is not null
drop table dbo.product_hist;
GO

create table dbo.product_hist(
    pht_id smallint not null identity(1,1),
    pro_id smallint not null,
    pht_date datetime not null,
    pht_cost decimal(7,2) not null check (pht_cost >= 0),
    pht_price decimal(7,2) not null check (pht_price >= 0),
    pht_discount decimal(3,0) null,
    pht_notes varchar(255) null,
    primary key (pht_id),

    constraint fk_product_hist_product
    foreign key (pro_id)
    references dbo.product (pro_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table order_line
-- -------------------------------------
if OBJECT_ID (N'dbo.order_line', N'U') is not null
drop table dbo.order_line;
GO

create table dbo.order_line(
    oln_id int not null identity(1,1),
    ord_id int not null,
    pro_id smallint not null,
    oln_qty smallint not null check (oln_qty >= 0),
    oln_price decimal(7,2) not null check (oln_price >= 0),
    oln_notes varchar(255) null,
    primary key (oln_id),

    constraint fk_order_line_order
    foreign key (ord_id)
    references dbo.[order] (ord_id)
    on delete cascade
    on update cascade,

    constraint fk_order_line_product
    foreign key (pro_id)
    references dbo.product (pro_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table time
-- -------------------------------------

IF OBJECT_ID (N'dbo.time', N'U') IS NOT NULL
DROP TABLE dbo.time;
GO

CREATE TABLE dbo.time(
    tim_id INT NOT NULL identity(1,1),
    tim_yr SMALLINT NOT NULL, -- 2 byte integer (no YEAR data type in MS SQL Server)
    tim_qtr TINYINT NOT NULL, -- 1 - 4 
    tim_month TINYINT NOT NULL, -- 1 - 12 
    tim_week TINYINT NOT NULL, -- 1 - 52
    tim_day TINYINT NOT NULL, -- 1 - 7
    tim_time TIME NOT NULL, -- based on 24-hour clock
    tim_notes VARCHAR(255) NULL,
    PRIMARY KEY (tim_id)
);
GO

-- -------------------------------------
-- Table sale
-- -------------------------------------

IF OBJECT_ID (N'dbo.sale', N'U') IS NOT NULL
DROP TABLE dbo.sale;
GO

CREATE TABLE dbo.sale(
    pro_id SMALLINT NOT NULL,
    str_id SMALLINT NOT NULL,
    cnt_id INT NOT NULL,
    tim_id INT NOT NULL,
    sal_qty SMALLINT NOT NULL,
    sal_price DECIMAL(8,2) NOT NULL,
    sal_total DECIMAL(8,2) NOT NULL,
    sal_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id, cnt_id, tim_id, str_id),

    CONSTRAINT ux_pro_id_str_id_cnt_id_tim_id
    unique nonclustered (pro_id ASC, str_id ASC, cnt_id ASC, tim_id ASC),

    CONSTRAINT fk_sale_time
    FOREIGN KEY (tim_id)
    REFERENCES dbo.time (tim_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_sale_contact
    FOREIGN KEY (cnt_id)
    REFERENCES dbo.contact (cnt_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_sale_store
    FOREIGN KEY (str_id)
    REFERENCES dbo.store (str_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_sale_product
    FOREIGN KEY (pro_id)
    REFERENCES dbo.product (pro_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

Select * from information_schema.tables;

-- -------------------------------------
-- DATA person
-- -------------------------------------
insert into dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
values
(1, NULL, 'Ava', 'Adams', 'f', '1960-10-13', '123 Apple Dr.', 'Atlanta', 'GA', 111111111, 'aa@gmail.com', 's', NULL), 
(2, NULL, 'Bob', 'Brown', 'm', '1969-04-02', '234 Banana Dr.', 'Boston', 'MA', 222222222, 'bb@gmail.com', 's', NULL), 
(3, NULL, 'Mia', 'Khalifa', 'f', '1999-09-10', '345 Coconut St.', 'Columbus', 'IO', 333333333, 'mk@gmail.com', 's', NULL), 
(4, NULL, 'Jane', 'Doe', 'f', '1980-11-08', '456 Dragonfruit Dr.', 'Detroit', 'MI', 444444444, 'jd@gmail.com', 's', NULL), 
(5, NULL, 'Mick', 'Blue', 'm', '1995-08-20', '567 Eclair Ln.', 'Jacksonville', 'FL', 555555555, 'mb@gmail.com', 's', NULL), 
(6, NULL, 'Lisa', 'Ann', 'f', '1999-05-05', '678 Fruity Ave.', 'Tallahassee', 'FL', 666666666, 'la@gmail.com', 'c', NULL), 
(7, NULL, 'Lena', 'Paul', 'f', '1980-10-10', '789 Gravy St.', 'Clearwater', 'FL', 777777777, 'lp@gmail.com', 'c', NULL), 
(8, NULL, 'Cory', 'Chase', 'm', '1995-03-10', '890 Handy Ave.', 'Tampa', 'FL', 888888888, 'cc@gmail.com', 'c', NULL), 
(9, NULL, 'Jonny', 'Sins', 'm', '1997-07-20', '901 Indigo Ave.', 'Fort Myers', 'FL', 999999999, 'js@gmail.com', 'c', NULL), 
(10, NULL, 'Phil', 'Magroin', 'm', '1987-10-02', '987 Juice Ln.', 'Key West', 'FL', 123456789, 'pm@gmail.com', 'c', NULL), 
(11, NULL, 'Hue', 'Jainis', 'm', '1970-11-15', '876 Karma Ave.', 'Miami', 'FL', 234567891, 'hj@gmail.com', 'c', NULL), 
(12, NULL, 'Pat', 'Muhiney', 'm', '1988-11-22', '765 Love St.', 'Orlando', 'FL', 345678912, 'pm@gmail.com', 'c', NULL), 
(13, NULL, 'Karlee', 'Grey', 'f', '1995-01-03', '654 Money Dr.', 'St. Pete', 'FL', 456789123, 'kg@gmail.com', 'c', NULL), 
(14, NULL, 'Mike', 'Oxlong', 'm', '1963-04-12', '543 Nobody Ct.', 'Jacksonville', 'FL', 567891234, 'mo@gmail.com', 'c', NULL), 
(15, NULL, 'Jenna', 'Tolls', 'f', '1979-05-10', '432 Octopus Blvd.', 'Tallahassee', 'FL', 567891234, 'jt@gmail.com', 'c', NULL); 
GO 

Select * from person;

-- -------------------------------------
-- DATA phone
-- -------------------------------------
insert into dbo.phone
( per_id, phn_num, phn_type, phn_notes)
values
(5, 5555555555, 'h', null), 
(4, 4444444444, 'h', null), 
(3, 3333333333, 'h', null), 
(2, 2222222222, 'h', null), 
(1, 1111111111, 'h', null) 
GO 

Select * from phone;

-- -------------------------------------
-- DATA slsrep
-- -------------------------------------
insert into dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
values
(1, 150000, 90000, 2000, null), 
(2, 80000, 35000, 3000, null), 
(3, 150000, 85000, 9000, null), 
(4, 130000, 87000, 15000, null), 
(5, 99000, 32000, 9000, null)
GO 

Select * from slsrep;

-- -------------------------------------
-- DATA customer
-- -------------------------------------
insert into dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
values
(6, 150, 14000, null), 
(7, 99, 250, null), 
(8, 0, 5000, null), 
(9, 999, 2000, null), 
(10, 500, 750, null)
GO 

Select * from customer;

-- -------------------------------------
-- DATA contact
-- -------------------------------------
insert into dbo.contact
(per_sid, per_cid, cnt_date, cnt_notes)
values
(1, 6, '1999-05-05', null), 
(2, 7, '1995-04-04', null), 
(3, 8, '2007-03-04', null), 
(4, 9, '2010-07-08', null), 
(5, 10, '2012-10-05', null)
GO 

Select * from contact;

-- -------------------------------------
-- DATA [order]
-- -------------------------------------
insert into dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
values
(1, '2005-01-03', '2005-01-07', null), 
(2, '2011-03-05', '2011-03-10', null), 
(3, '2008-05-05', '2008-05-15', null), 
(4, '2012-08-10', '2012-08-20', null), 
(5, '2010-12-25', '2011-01-04', null)
GO 

Select * from [order];

-- -------------------------------------
-- DATA region
-- -------------------------------------
INSERT INTO region
(reg_name, reg_notes)
VALUES
('n', NULL),
('e', NULL),
('s', NULL),
('w', NULL),
('c', NULL);
GO

select * from dbo.region;

-- -------------------------------------
-- DATA state
-- -------------------------------------
INSERT INTO state
(reg_id, ste_name, ste_notes)
VALUES
(1, 'FL', NULL),
(3, 'GA', NULL),
(4, 'CO', NULL),
(5, 'AK', NULL),
(2, 'TX', NULL);
GO

select * from dbo.state;

-- -------------------------------------
-- DATA city
-- -------------------------------------
INSERT INTO city
(ste_id, city_name, city_notes)
VALUES
(1, 'Tallahassee', NULL),
(2, 'Atlanta', NULL),
(2, 'Denver', NULL),
(3, 'Anchorage', NULL),
(4, 'Houston', NULL);
GO

select * from dbo.city;

-- -------------------------------------
-- DATA store
-- -------------------------------------
insert into dbo.store
(str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
values
('Walgreens', '1234 Zebra Ln.', 'Denver', 'CO', 121212121, 1231231231, 'walgreens@gmail.com', 'https://www.walgreens.com', null), 
('CVS', '2345 Elephant Rd.', 'Chicago', 'IL', 232323232, 2342342342, 'cvs@gmail.com', 'https://www.cvs.com', null), 
('Home Depot', '3456 Cat Ave.', 'Seattle', 'WA', 343434343, 3453453453, 'homedepot@gmail.com', 'https://www.homedepot.com', null), 
('Walmart', '4567 Whale Ln.', 'Tampa', 'FL', 454545454, 4564564564, 'walmart@gmail.com', 'https://www.walmart.com', null), 
('Dollar Tree', '5678 Dolphin Rd.', 'Tallahassee', 'FL', 565656565, 5675675675, 'dollartree@gmail.com', 'https://www.dollartree.com', null)
GO 

Select * from store;

-- -------------------------------------
-- DATA invoice
-- -------------------------------------
insert into dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
values
(5, 1, '2000-03-03', 50, 0, null), 
(4, 1, '2011-11-11', 100, 0, null), 
(3, 2, '2010-10-10', 69, 1, null), 
(2, 4, '1999-05-05', 99.99, 1, null), 
(1, 5, '2009-12-25', 1000, 0, null)
GO 

Select * from invoice;

-- -------------------------------------
-- DATA vendor
-- -------------------------------------
insert into dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
values
('Dalooth', '987 Applesauce Ct.', 'Tallahassee', 'FL', 987654321, 1023456789, 'dalooth@gmail.com', 'https://www.dalooth.com', null), 
('GAP', '876 Beetle Rd.', 'Jacksonville', 'FL', 876543219, 1203456789, 'gap@gmail.com', 'https://www.gap.com', null), 
('Ralph Lauren', '765 Crayon Ave.', 'Orlando', 'FL', 765432198, 1230456789, 'ralphlauren@gmail.com', 'https://www.ralphlauren.com', null), 
('Versace', '654 Donkey Ln.', 'Tampa', 'FL', 654321987,1234056789, 'versace@gmail.com', 'https://www.versace.com', null), 
('Lucky', '543 Everet Rd.', 'Miami', 'FL', 543219876, 1234506789, 'lucky@gmail.com', 'https://www.lucky.com', null)
GO 

Select * from vendor;

-- -------------------------------------
-- DATA product
-- -------------------------------------
insert into dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
values
(1, 'hat', null, 0.5, 50, 4.99, 7.99, null, null), 
(2, 'shirt', null, 1.5, 100, 14.99, 24.99, null, null), 
(3, 'pants', null, 2.5, 50, 29.99, 39.99, null, null), 
(4, 'socks', null, 0.5, 25, 4.99, 14.99, null, null), 
(5, 'shoes', null, 1.5, 150, 39.99, 49.99, null, null)
GO 

Select * from product;

-- -------------------------------------
-- DATA order_line
-- -------------------------------------
insert into dbo.order_line
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
values
(1, 2, 15, 29.99, null), 
(2, 3, 9, 15.99, null), 
(3, 4, 4, 9.99, null), 
(5, 1, 5, 14.99, null), 
(4, 5, 15, 59.99, null)
GO 

Select * from order_line;

-- -------------------------------------
-- DATA payment
-- -------------------------------------
insert into dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
values
(5, '2009-05-05', 4.99, null), 
(4, '2010-08-03', 9.99, null), 
(1, '2011-07-05', 14.99, null), 
(3, '2005-09-22', 19.99, null), 
(2, '2006-04-20', 29.99, null)
GO 

Select * from payment;

-- -------------------------------------
-- DATA product_hist
-- -------------------------------------
insert into dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
values
(1, '2005-04-11 11:53:30', 4.99, 9.99, null, null), 
(2, '2002-03-04 23:22:22', 9.99, 14.99, null, null), 
(3, '1999-05-05 09:10:45', 14.99, 24.99, null, null), 
(4, '2004-11-20 13:18:25', 19.99, 29.99, null, null), 
(5, '2006-07-21 18:18:18', 9.99, 39.99, null, null)
GO 

Select * from product_hist;

-- -------------------------------------
-- DATA time
-- -------------------------------------
INSERT INTO time
(tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes)
VALUES
(2012, 1, 10, 40, 7, '20:11:22', NULL),
(2013, 4, 5, 19, 5, '12:30:24', NULL),
(2016, 3, 1, 3, 3, '10:28:30', NULL),
(2015, 2, 7, 32, 5, '11:42:16', NULL),
(2011, 2, 11, 47, 9, '04:50:10', NULL),
(2010, 1, 12, 50, 8, '02:46:56', NULL),
(2000, 3, 9, 37, 4, '21:43:30', NULL),
(2007, 4, 8, 34, 1, '08:46:22', NULL),
(1999, 3, 4, 15, 6, '04:25:37', NULL),
(2005, 2, 2, 8, 3, '10:36:24', NULL),
(2003, 1, 5, 20, 5, '10:54:55', NULL);
GO

Select * from dbo.time;

-- -------------------------------------
-- DATA sale
-- -------------------------------------
INSERT INTO sale
(pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes)
VALUES
(1, 2, 3, 9, 14, 13.99, 19.99, NULL),
(1, 3, 2, 10, 7, 1.99, 7.99, NULL),
(2, 4, 2, 7, 11, 2.99, 24.99, NULL),
(1, 4, 3, 4, 21, 19.99, 29.99, NULL),
(3, 4, 2, 6, 8, 6.99, 36.99, NULL),
(3, 5, 4, 3, 9, 7.99, 99.99, NULL),
(4, 4, 3, 9, 5, 1.99, 7.99, NULL),
(1, 2, 2, 8, 5, 8.99, 24.99, NULL),
(4, 3, 4, 4, 3, 7.99, 19.99, NULL),
(3, 1, 2, 6, 7, 9.99, 30.99, NULL),
(4, 4, 5, 3, 2, 9.99, 19.99, NULL),
(3, 2, 4, 10, 15, 4.99, 19.99, NULL),
(2, 4, 3, 6, 5, 6.99, 14.99, NULL),
(5, 5, 4, 9, 2, 4.99, 30.99, NULL),
(3, 1, 2, 7, 11, 3.99, 8.99, NULL),
(2, 4, 5, 5, 16, 1.99, 3.99, NULL),
(5, 2, 5, 3, 5, 8.99, 24.99, NULL),
(3, 5, 2, 7, 13, 6.99, 11.99, NULL),
(4, 2, 3, 6, 6, 5.99, 6.99, NULL),
(3, 1, 4, 5, 6, 1.99, 14.99, NULL),
(1, 3, 2, 2, 5, 8.99, 29.99, NULL),
(1, 4, 4, 1, 3, 3.99, 19.99, NULL),
(2, 1, 4, 4, 15, 5.99, 22.99, NULL),
(2, 4, 3, 3, 12, 8.99, 24.99, NULL),
(4, 4, 2, 10, 11, 3.99, 17.99, NULL);
GO

Select * from dbo.sale;

-- -------------------------------------
-- DATA srp_hist
-- -------------------------------------
insert into dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_ytd_sales, sht_ytd_comm, sht_notes)
values
(1, 'i', getDate(), system_user, getDate(), 150000, 110000, 11000, null), 
(2, 'i', getDate(), system_user, getDate(), 100000, 175000, 17500, null), 
(3, 'u', getDate(), system_user, getDate(), 150000, 100000, 10000, null), 
(4, 'u', getDate(), original_login(), getDate(), 250000, 200000, 20000, null), 
(5, 'i', getDate(), original_login(), getDate(), 250000, 200000, 20000, null)
GO 

Select * from srp_hist;

-- -------------------------------------
-- REPORTS 1
-- -------------------------------------
if OBJECT_ID (N'dbo.product_days_of_week', N'P') is not null
drop proc dbo.product_days_of_week;
go

create proc dbo.product_days_of_week as
begin
    select pro_name, pro_descript, datename(dw, tim_day-1) 'day_of_week'
    from product p
    join sale s on p.pro_id=s.pro_id
    join time t on t.tim_id=s.tim_id
    order by tim_day-1 asc;
end
go

exec dbo.product_days_of_week;


-- -------------------------------------
-- REPORTS 2
-- -------------------------------------
if OBJECT_ID (N'dbo.product_down', N'P') is not null
drop proc dbo.product_down;
go

create proc dbo.product_down as
begin
    select pro_name, pro_qoh,
    format(pro_cost, 'C', 'en-us') as cost,
    format(pro_price, 'C', 'en-us') as price,
    str_name, city_name, ste_name, reg_name
    from product p
    join sale s on p.pro_id=s.pro_id
    join store sr on sr.str_id=s.str_id
    join city c on sr.city_id=c.city_id
    join state st on c.ste_id=st.ste_id
    join region r on st.reg_id=r.reg_id
    order by pro_qoh desc;
end
go

exec dbo.product_down;

-- -------------------------------------
-- REPORTS 3
-- -------------------------------------
if OBJECT_ID (N'dbo.add_payment', N'P') is not null
drop proc dbo.add_payment;
go

create proc dbo.add_payment
    @inv_id_p int,
    @pay_date_p datetime,
    @pay_amt_p decimal(7,2),
    @pay_notes_p varchar(255)
as
begin
    insert into payment (inv_id, pay_date, pay_amt, pay_notes)
    values
    (@inv_id_p, @pay_date_p, @pay_amt_p, @pay_notes_p);
end
go

select * from payment

declare
@inv_id_v int = 5,
@pay_date_v datetime = '2014-01-05 11:56:38',
@pay_amt_v decimal(7,2) = 159.99,
@pay_notes_v varchar(255) = 'testing add_payment';

exec dbo.add_payment @inv_id_v, @pay_date_v, @pay_amt_v, @pay_notes_v;

select * from payment;

-- -------------------------------------
-- REPORTS 4
-- -------------------------------------
if OBJECT_ID (N'dbo.customer_balance', N'P') is not null
drop proc dbo.customer_balance;
go

create proc dbo.customer_balance
    @per_lname_p varchar(30)
as
begin
    select p.per_id, per_fname, per_lname, i.inv_id,
    format(sum(pay_amt), 'C', 'en-us') as total_paid,
    format((inv_total - sum(pay_amt)), 'C', 'en-us') as invoice_diff
    from person p
    join dbo.customer c on p.per_id=c.per_id
    join dbo.contact ct on c.per_id=ct.per_id
    join dbo.[order] o on ct.cnt_id=o.cnt_id
    join dbo.invoice i on o.ord_id=i.ord_id
    join dbo.payment pt on i.inv_id=pt.inv_id
    where per_lname=@per_lname_p
    group by p.per_id, i.inv_id, per_lname, per_fname, inv_total;
end
go

declare
@per_lname_v varchar(30) = 'smith';

exec dbo.customer_balance @per_lname_v;

-- -------------------------------------
-- REPORTS 5
-- -------------------------------------
if OBJECT_ID (N'dbo.store_sales_between_dates', N'P') is not null
drop proc dbo.store_sales_between_dates;
go

create proc dbo.store_sales_between_dates
    @start_date_p smallint,
    @end_date_p smallint
as
begin
    select st.str_id, format(sum(sal_total), 'C', 'en-us') as total_sales, tim_yr as year
    from store st
    join sale s on st.str_id=s.str_id
    join time t on s.tim_id=t.tim_id
    where tim_yr between @start_date_p and @end_date_p
    group by tim_yr, st.str_id
    order by sum(sal_total) desc, tim_yr desc;
end
go

declare
@start_date_v smallint = 2010,
@end_date_v smallint = 2013;


exec dbo.store_sales_between_dates @start_date_v, @end_date_v;

-- -------------------------------------
-- REPORTS 6
-- -------------------------------------
if OBJECT_ID(N'dbo.trg_check_inv_paid', N'TR') is not null
drop trigger dbo.trg_check_inv_paid
go

create trigger dbo.trg_check_inv_paid
on dbo.slsrep
after insert as
begin
    update invoice
    set inv_paid=0;

    update invoice
    set inv_paid=1
    from invoice as i
        join (
            select inv_id, sum(pay_amt) as total_paid
            from payment
            group by inv_id
        ) as v on i.inv_id=v.inv_id
        where total_paid >= inv_total;
end
go

select * from invoice;

insert into dbo.invoice
(inv_id, inv_date, inv_total, inv_notes)
values
(6, '2014-07-04', 75.00, 'paid by check');

select * from invoice;

select * from payment;

select inv_id, sum(pay_amt) as sum_pmt
from payment
group by inv_id;

-- -------------------------------------
-- REPORTS Extra Credit
-- -------------------------------------
if OBJECT_ID (N'dbo.order_line_total', N'P') is not null
drop proc dbo.order_line_total;
go

create proc dbo.order_line_total as
begin
    select oln_id, p.pro_id, pro_name, pro_descript,
    format(pro_price, 'C', 'en-us') as pro_price,
    oln_qty,
    format((oln_qty * pro_price), 'C', 'en-us') as oln_price,
    format((oln_qty * pro_price) * 1.06, 'C', 'en-us') as total_with_6pct_tax
    from product p
    join order_line ol on p.pro_id=ol.pro_id
    order by p.pro_id
end
go

exec dbo.order_line_total;