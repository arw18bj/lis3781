# LIS3781 Advanced Database Management

## Avery Withers

### Assignment 4 Requirements:

*Four Parts:*

1. Open MS SQL Server
2. Create tables & inserts
3. Execute code
4. Take screenshot of ERD

#### README.md file should include the following items:

* Screenshot of ERD
* Optional: SQL code for the required reports
* Bitbucket repo link

#### Assignment Screenshots:

*Screenshot of A4 ERD*:

![A4 ERD](img/a4_erd.png)