# LIS3781 Advanced Database Management

## Avery Withers

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram, and SQL Code (optional)
5. Bitbucket repo links:

    a) https://bitbucket.org/arw18bj/lis3781/src/3cdd305ea25c554d061383cb126b903c463ce4ce/a1/a1_README.md

    b) https://bitbucket.org/arw18bj/bitbucketstationlocations/src/master/

#### README.md file should include the following items:

* Screenshot of A1 ERD
* Ex1. SQL Solution
* git commands w/short descriptions


#### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git fetch - Download objects and refs from another repository

#### Assignment Screenshots:

*Screenshot of A1 ERD*:

![A1 ERD](img/a1_erd.png)

*Screenshot of A1 ex1*:

![A1 ex1](img/a1_ex1.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/arw18bj/bitbucketstationlocations/ "Bitbucket Station Locations")