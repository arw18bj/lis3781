# LIS3781 Advanced Database Management

## Avery Withers

### Assignment 3 Requirements:

*Four Parts:*

1. Log into Oracle and connect to CCI server
2. Create tables, inserts using Oracle
3. Provide screenshots of code and populated tables
4. Forward engineer to CCI server

#### README.md file should include the following items:

* Screenshot of SQL code used to create and populate tables 
* Screenshot of populated tables (w/in the Oracle environment) 

#### Assignment Screenshots:

*Screenshot of A3 Code*:

![A2 Code](img/a3_code.png)

*Screenshot of A3 Query Result 1*:

![A3 First QR](img/a3_qr1.png)

*Screenshot of A3 Query Result 2*:

![A3 Second QR](img/a3_qr2.png)

*Screenshot of A3 Query Result 3*:

![A3 Third QR](img/a3_qr3.png)